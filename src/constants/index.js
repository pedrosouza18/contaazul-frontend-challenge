export const REQUEST_STATUS = {
  RESOLVED: 'resolved',
  REJECTED: 'rejected',
  PENDING: 'pending',
};
