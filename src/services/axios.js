import axios from 'axios';

const { ROOT } = API;

const instance = axios.create({
  baseURL: ROOT,
});

export default instance;
