import axios from '../axios';
import { REQUEST_STATUS } from 'constants/index';
import { localStorage } from 'utils/index';

const CITIES = ['Nuuk', 'Urubici', 'Nairobi'];

const sortCitiesByTempeture = (cities) => {
  return cities.sort((a, b) => a?.temp - b?.temp);
};

const buildCityProps = (data) => {
  const { name, sys, main } = data;
  return {
    name: `${name}, ${sys.country}`,
    ...main,
    tempeture: main.temp,
    last_update: new Date().getTime(),
    status: REQUEST_STATUS.RESOLVED,
  };
};

const buildCities = (res) => {
  return sortCitiesByTempeture(
    res.map((result) => {
      const { value, status } = result;
      return {
        ...(value && { ...value }),
        status:
          status === REQUEST_STATUS.REJECTED ? REQUEST_STATUS.REJECTED : REQUEST_STATUS.RESOLVED,
      };
    })
  ).reduce((prev, item, index) => {
    return {
      ...prev,
      [CITIES[index].toLowerCase()]: item,
    };
  }, {});
};

const getCachedResults = () => {
  const cachedResults = localStorage.getItem('cities');
  if (
    cachedResults &&
    Object.values(cachedResults).find(({ last_update }) => Math.max(last_update)).last_update <
      new Date().getTime()
  )
    return cachedResults;
  return null;
};

async function getCity(cityName) {
  return axios.get(`?q=${cityName}&appid=${WEATHER_KEY}&units=metric`).then(({ data }) => {
    const cachedResults = localStorage.getItem('cities');
    const result = buildCityProps(data);
    if (cachedResults) {
      cachedResults[cityName] = result;
      localStorage.setItem('cities', cachedResults);
    }
    return result;
  });
}

async function getCities() {
  const cachedResults = getCachedResults();
  if (cachedResults) return cachedResults;

  const mappedCities = CITIES.map((city) => getCity(city));
  try {
    const res = await Promise.allSettled(mappedCities);
    const convertedResults = buildCities(res);
    localStorage.setItem('cities', convertedResults);
    return convertedResults;
  } catch (e) {
    throw new error(e);
  }
}

export default { getCity, getCities };
