import '@testing-library/jest-dom/extend-expect';
import { server } from 'mocks/server';

const localStorageMock = (function () {
  let store = {};
  return {
    getItem: function (key) {
      return store[key];
    },
    setItem: function (key, value) {
      store[key] = value.toString();
    },
    clear: function () {
      store = {};
    },
    removeItem: function (key) {
      delete store[key];
    },
  };
})();

beforeAll(() => {
  Object.defineProperty(window, 'localStorage', { value: localStorageMock });
  server.listen();
});

afterEach(() => server.resetHandlers());

afterAll(() => server.close());
