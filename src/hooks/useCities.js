import { useReducer, useEffect, useRef, useCallback } from 'react';
import services from 'services';
import { localStorage } from 'utils/index';

const tenMinutesInMilliseconds = 10 * 60000;

function asyncReducer(state, action) {
  switch (action.type) {
    case 'change': {
      return { ...state, cities: { ...action.value } };
    }
    default: {
      throw new Error(`Unhandled type: ${action.type}`);
    }
  }
}

export default function useCities(initialState = {}) {
  const timeoutId = useRef(null);
  const lastUpdate = useRef(null);
  const [state, dispatch] = useReducer(asyncReducer, {
    ...initialState,
    cities: {
      nairobi: null,
      nuuk: null,
      urubici: null,
    },
  });

  const tryAgain = (cityName) => {
    dispatch({
      type: 'change',
      value: { ...state.cities, [cityName]: { status: 'pending' } },
    });
    services.city
      .getCity(cityName)
      .then((res) => {
        dispatch({ type: 'change', value: { ...state.cities, [cityName]: res } });
        pollingGetCities();
      })
      .catch(() => {
        dispatch({
          type: 'change',
          value: { ...state.cities, [cityName]: { status: 'rejected' } },
        });
      });
  };

  const pollingGetCities = () => {
    timeoutId.current = setTimeout(function timeout() {
      clearTimeout(timeoutId.current);
      if (new Date().getTime() > lastUpdate.current + tenMinutesInMilliseconds) {
        localStorage.setItem('cities', null);
        return getCities();
      }

      timeoutId.current = setTimeout(timeout, 2000);
    }, 2000);
  };

  const getCities = useCallback(() => {
    services.city.getCities().then((res) => {
      const resultsToArray = Object.values(res);
      dispatch({
        type: 'change',
        value: res,
      });
      lastUpdate.current =
        resultsToArray.find(({ last_update }) => Math.max(last_update))?.last_update ??
        new Date().getTime();
      if (resultsToArray.every(({ status }) => status === 'resolved')) pollingGetCities();
    });
  }, []);

  useEffect(() => {
    getCities();

    return () => {
      clearTimeout(timeoutId.current);
      timeoutId.current = null;
      lastUpdate.current = null;
    };
  }, [getCities]);

  return {
    ...state,
    tryAgain,
  };
}
