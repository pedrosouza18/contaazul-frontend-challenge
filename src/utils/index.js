export const localStorage = (() => {
  const getItem = (key) => {
    try {
      const item = window.localStorage.getItem(key);
      return typeof item === 'string' ? JSON.parse(item) : null;
    } catch (e) {
      return {};
    }
  };

  const setItem = (key, value) => {
    window.localStorage.setItem(key, JSON.stringify(value));
  };

  return { getItem, setItem };
})();
