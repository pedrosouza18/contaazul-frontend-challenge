export const cities = {
  nuuk: {
    coord: {
      lon: -51.7216,
      lat: 64.1835,
    },
    weather: [
      {
        id: 804,
        main: 'Clouds',
        description: 'overcast clouds',
        icon: '04n',
      },
    ],
    base: 'stations',
    main: {
      temp: -14.89,
      feels_like: -21.19,
      temp_min: -14.89,
      temp_max: -14.89,
      pressure: 1002,
      humidity: 94,
      sea_level: 1002,
      grnd_level: 1000,
    },
    visibility: 10000,
    wind: {
      speed: 2.86,
      deg: 68,
      gust: 2.96,
    },
    clouds: {
      all: 100,
    },
    dt: 1642801802,
    sys: {
      country: 'GL',
      sunrise: 1642768777,
      sunset: 1642790184,
    },
    timezone: -10800,
    id: 3421319,
    name: 'Nuuk',
    cod: 200,
  },
  urubici: {
    coord: {
      lon: -49.5917,
      lat: -28.015,
    },
    weather: [
      {
        id: 804,
        main: 'Clouds',
        description: 'overcast clouds',
        icon: '04d',
      },
    ],
    base: 'stations',
    main: {
      temp: 22.41,
      feels_like: 22.8,
      temp_min: 22.41,
      temp_max: 22.41,
      pressure: 1014,
      humidity: 80,
      sea_level: 1014,
      grnd_level: 912,
    },
    visibility: 10000,
    wind: {
      speed: 1.09,
      deg: 128,
      gust: 1.82,
    },
    clouds: {
      all: 90,
    },
    dt: 1642801909,
    sys: {
      country: 'BR',
      sunrise: 1642754462,
      sunset: 1642803476,
    },
    timezone: -10800,
    id: 3445709,
    name: 'Urubici',
    cod: 200,
  },
  nairobi: {
    coord: {
      lon: 36.8167,
      lat: -1.2833,
    },
    weather: [
      {
        id: 803,
        main: 'Clouds',
        description: 'broken clouds',
        icon: '04n',
      },
    ],
    base: 'stations',
    main: {
      temp: 17.93,
      feels_like: 17.92,
      temp_min: 17.93,
      temp_max: 18.31,
      pressure: 1021,
      humidity: 82,
    },
    visibility: 10000,
    wind: {
      speed: 0,
      deg: 0,
    },
    clouds: {
      all: 75,
    },
    dt: 1642800639,
    sys: {
      type: 1,
      id: 2558,
      country: 'KE',
      sunrise: 1642822722,
      sunset: 1642866566,
    },
    timezone: 10800,
    id: 184745,
    name: 'Nairobi',
    cod: 200,
  },
};
