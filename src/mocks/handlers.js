import { rest } from 'msw';
import { cities } from './cities';

export const handlers = [
  rest.get('*/weather', (req, res, ctx) => {
    const query = req.url.searchParams;
    const q = query.get('q');
    if (q === 'Nairobi') return res.networkError('There was an error');
    return res(ctx.status(200), ctx.json(cities[q.toLowerCase()]));
  }),
];
