import { screen, render } from '@testing-library/react';
import useEvent from '@testing-library/user-event';
import Card from 'components/card';

describe('Card component', () => {
  const cardProps = {
    cityKey: 'nuuk',
    name: 'Nuuk, GL',
    tempeture: 23.7,
    last_update: new Date().getTime(),
    humidity: 94,
    pressure: 1002,
  };

  it('should render highlighted card and show pressure and humidity infos', async () => {
    render(<Card {...cardProps} status="resolved" isHighlighted={true} />);

    expect(screen.getByRole('heading', { name: 'Nuuk, GL' })).toBeInTheDocument();
    expect(screen.getByText(/23.7/i)).toBeInTheDocument();
    expect(screen.getByText(/humidity/i)).toBeInTheDocument();
    expect(screen.getByText(/pressure/i)).toBeInTheDocument();
  });

  it('should show loader component', async () => {
    render(<Card status="pending" />);

    expect(screen.getByLabelText('carregando')).toBeInTheDocument();
  });

  it('should show error components and call try again', async () => {
    const tryAgain = jest.fn();

    render(<Card status="rejected" tryAgain={tryAgain} />);

    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.getByText('Something went wrong')).toBeInTheDocument();

    const tryAgainButton = screen.getByRole('button', { name: /try again/i });
    expect(tryAgainButton).toBeInTheDocument();

    useEvent.click(tryAgainButton);

    expect(tryAgain).toHaveBeenCalled();
    expect(tryAgain).toHaveBeenCalledTimes(1);
  });
});
