import { screen, render, waitForElementToBeRemoved, waitFor } from '@testing-library/react';
import useEvent from '@testing-library/user-event';
import Home from 'pages/home';

describe('Weather component', () => {
  it('should render climates from Nuuk and Urubici and cache data', async () => {
    window.localStorage.setItem = jest.fn();

    render(<Home />);

    const loaders = await screen.findAllByLabelText('carregando');
    expect(loaders).toHaveLength(3);

    await waitForElementToBeRemoved(() => expect(screen.getAllByLabelText('carregando')));

    await waitFor(() => {
      const cityNames = screen.getAllByRole('heading', { level: 2 });
      const tempetures = screen.getAllByTestId('tempeture');
      const mappedCityNames = cityNames.map((el) => el.textContent);
      const mappedTempetures = tempetures.map((el) => el.textContent);

      expect(mappedCityNames).toMatchInlineSnapshot(`
        Array [
          "Nuuk, GL",
          "Urubici, BR",
          "---",
        ]
      `);

      expect(mappedTempetures).toMatchInlineSnapshot(`
        Array [
          "-14.9",
          "22.4",
        ]
      `);
    });

    expect(window.localStorage.setItem).toHaveBeenCalled();
  });

  it('should return an error from Nairobi', async () => {
    render(<Home />);

    const loaders = await screen.findAllByLabelText('carregando');
    expect(loaders).toHaveLength(3);

    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeInTheDocument();
      expect(screen.getByRole('heading', { name: /nuuk/i })).toBeInTheDocument();
      expect(screen.getByRole('heading', { name: /urubici/i })).toBeInTheDocument();
      expect(screen.queryByRole('heading', { name: /nairobi/i })).toBeNull();
    });
  });

  it('should return an error and then click try again and then show nairobi', async () => {
    render(<Home />);

    const loaders = await screen.findAllByLabelText('carregando');
    expect(loaders).toHaveLength(3);

    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeInTheDocument();
      expect(screen.getByText('Something went wrong')).toBeInTheDocument();
    });

    const tryAgain = await screen.findByRole('button', { name: /try again/i });
    expect(tryAgain).toBeInTheDocument();

    useEvent.click(tryAgain);

    expect(await screen.findByLabelText('carregando')).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByRole('heading', { name: /nairobi/i })).toBeInTheDocument();
      expect(screen.getByText(/17.9/i)).toBeInTheDocument();
    });
  });
});
