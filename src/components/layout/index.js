import Head from 'components/head';
import Header from 'components/header';

export default function Layout({ children }) {
  return (
    <>
      <Head />
      <Header />
      <main>{children}</main>
    </>
  );
}
