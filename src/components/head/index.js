import { NextSeo } from 'next-seo';

export default function HeadComponent() {
  return (
    <>
      <NextSeo
        title="Conta Azul Front End Challenge"
        description="Webapp to show city weathers"
        additionalLinkTags={[
          {
            rel: 'icon',
            href: '/favicon.png',
          },
        ]}
      />
    </>
  );
}
