import LoaderIcon from 'icons/loader.svg';
import styles from './loader.module.scss';

export default function Loader() {
  return (
    <div className={styles.loader} aria-relevant="all" aria-live="polite" aria-label="carregando">
      <LoaderIcon aria-hidden="true" />
    </div>
  );
}
