import Logo from 'icons/logo.svg';
import styles from './header.module.scss';

export default function Header() {
  return (
    <header className={styles.header}>
      <Logo />
    </header>
  );
}
