import classnames from 'classnames';
import Loader from 'components/loader';
import Error from 'components/error';
import styles from './card.module.scss';

export default function Card({
  cityKey,
  name,
  tempeture,
  last_update,
  humidity,
  pressure,
  status,
  tryAgain,
  isHighlighted = false,
}) {
  const tempetureToNumber = Number(tempeture);
  const isComfort = tempetureToNumber > 5 && tempetureToNumber <= 25;
  const amountOfTempeture = tempetureToNumber <= 5 ? '-cold' : isComfort ? '-comfort' : '-hot';

  return (
    <li className={classnames(styles.card, { '-is-highlighted': isHighlighted })}>
      <h2>{name || '---'}</h2>
      {(!status || status === 'pending') && <Loader />}
      {status === 'rejected' && <Error tryAgain={() => tryAgain(cityKey)} />}
      {status === 'resolved' && (
        <>
          <p
            data-testid="tempeture"
            className={classnames(styles.tempeture, {
              [amountOfTempeture]: amountOfTempeture,
            })}
          >
            {tempeture.toFixed(1)}
          </p>
          <div className={styles.footer}>
            {isHighlighted && (
              <div>
                <dl>
                  <dt>HUMIDITY</dt>
                  <dd>
                    {humidity}
                    <span>%</span>
                  </dd>
                </dl>
                <dl>
                  <dt>PRESSURE</dt>
                  <dd>
                    {pressure}
                    <span>hPa</span>
                  </dd>
                </dl>
              </div>
            )}
            <p>Updated at {new Date(last_update).toLocaleTimeString('en-US')}</p>
          </div>
        </>
      )}
    </li>
  );
}
