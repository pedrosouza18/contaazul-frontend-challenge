import styles from './error.module.scss';

export default function Error({ tryAgain }) {
  return (
    <div className={styles.error} role="alert">
      <p>Something went wrong</p>
      <button onClick={tryAgain}>Try Again</button>
    </div>
  );
}
