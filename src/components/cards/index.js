import Card from 'components/card';
import styles from './cards.module.scss';

export default function Cards({ cards, tryAgain }) {
  const highlightedTempeture = Math.min(
    ...cards?.map(([_, value]) => value?.tempeture).filter((val) => val > 5 && val <= 25)
  );

  return (
    <ul className={styles.cards}>
      {cards.map(([key, value]) => (
        <Card
          key={key}
          cityKey={key}
          {...value}
          tryAgain={tryAgain}
          isHighlighted={highlightedTempeture === value?.tempeture}
        />
      ))}
    </ul>
  );
}
