import Cards from 'components/cards';
import useCities from 'hooks/useCities';

export default function Home() {
  const { cities, tryAgain } = useCities();

  return (
    <section>
      <Cards cards={Object.entries(cities)} tryAgain={tryAgain} />
    </section>
  );
}
