<h1 align="center">Welcome to Conta Azul Weather App 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
</p>

### ✨ [Webapp](https://contaazul-frontend-challeng.herokuapp.com/)

## Requisitos

- [Node.js][node] 12+
- [npm][npm] (normally comes with Node.js)

[node]: https://nodejs.org/
[npm]: https://www.npmjs.com/

## Tecnologia utilizada

- [Next.js](https://nextjs.org/)
- [Sass (CSS modules)](https://sass-lang.com/)
- [Mock Service Worker](https://mswjs.io/)
- [CicleCI](https://circleci.com/)
- [Cypress](https://www.cypress.io/)
- [Jest](https://jestjs.io/pt-BR/)
- [Testing Library](https://testing-library.com/)
- Heroku

## Como inicializar a aplicação

Antes de tudo, rode `yarn` para instalar as dependências e após siga as instruções:

```sh
npm run dev
# or
yarn dev
```

## Organização de components

Components locais deverão permanecer dentro da própria `page/`, deixando a pasta `components/` para apenas os que forem utilizados em mais de um local. Dentro de `page/` é importante se atentar a usar apenas um arquivo `.js` ou `.jsx` pois o next interpreta como página, caso queira usar algum arquivo de extensão `.js` ou `.jsx`, sugiro colocar ou em um diretório `/modules` ou em algum ou diretório de sua preferência. 

```
pages/
  └── About/
    └── index.js
    └── index.module.scss
components/
  └── Header
    ├── index.module.scss
    └── index.js
services/
  ├── index.js
  └── auth.js
hooks/
  └── cards.js
styles
  └── mixins.scss
```

## Variáveis de ambiente

Para utilizar variáveis de ambiente dentro do projeto é necessário modificar o arquivo `env.js` dentro do diretório `env` na raiz do projeto:

```js
module.exports = {
  development: {
    API: {
      ROOT: 'https://xxxxx-your-api',
    },
  },
  staging: {
    API: {
      ROOT: 'https://xxxxx-your-api',
    },
  },
  production: {
    API: {
      ROOT: 'https://xxxxx-your-api',
    },
  },
};
```

```js
const { ROOT } = API;

const instance = axios.create({
  baseURL: ROOT,
  timeout: 2000,
});
```

## Services

Requests que necessitarem de mapeamento de dados ou forem reutilizáveis deverão ser colocadas dentro de `services/`. Requests simples poderão ser realizados dentro da própria lógica dos components.

## Testes

O projeto já vem configurado com [jest](https://jestjs.io/), [testing library](https://testing-library.com/) e [Mocking service worker](https://mswjs.io/)

```
__tests__/
  └── index.test.js
mocks/
  └── handlers.js
  └── characters.js
  └── server.js
setup-test-env.js
```

Para rodar os testes é só utilizar os seguintes comandos:

```bash
npm run test
npm run test:watch
# or
yarn test
yarn test:watch
```

## Assets

Imagens e fonts são colocadas dentro de `/public/static` e podem ser usadas apenas passando `/static/images` ou `static/fonts`. Para SVGs, existe uma pasta chamada `/icons` onde lá serão colocados todos os SVGs que serão usados no projeto. Para usar um SVG, basta importa-lo na página ou componente que irá ser usado e usa-lo como um componente React `<SVG />`.

Caso queira desabilitar o uso de images no formato do path por exemplo `<img src="/static/images.image.png" />` para usar via import, é só incluir o seguinte código no arquivo `next.config.js` e criar um diretório `src/images`:

```js
module.exports = {
    images: {
        disableStaticImages: true
    }
}
```

E importar no componente normalmente:

```js
import Imagexx from 'images/imagexxx.png'
```

## SEO

O next já tem por default um componente chamado `next/head` que por baixo dos panos nada mais é do que um reach helmet, onde lá pode se incluir toda a parte de SEO do projeto como metatags e tudo mais. Porém para uma praticidade, foi incluida a lib [next-seo](https://github.com/garmeeh/next-seo) onde nela você pode incluir via props várias configurações de SEO para o seu projeto. Caso não queira usa-la, basta usar o componente nativo do next o `next/head` e ir incluindo manualmente as metatags que deseja.

O next permite criar a tag `next/head` ou usar o `next-seo` por página, assim podendo customizar o title e outras coisas que desejar.

```js
import { NextSeo } from 'next-seo';

export default function HeadComponent() {
  return (
    <>
      <NextSeo
        title="Next React Boilerplate"
        description="Boilerplate to build fast webapps"
        canonical="https://www.canonical.ie/"
        additionalLinkTags={[
          {
            rel: 'icon',
            href: 'https://www.test.ie/favicon.ico',
          },
          {
            rel: 'apple-touch-icon',
            href: 'https://www.test.ie/touch-icon-ipad.jpg',
            sizes: '76x76',
          },
        ]}
      />
    </>
  );
}

```

## Commits

Para comitar com uma maior praticidade, é necessário rodar os seguintes comandos:

```bash
git add .
yarn commit
```

Após o `yarn commit`, irá aparecer uma lista de opções que resumem qual o tipo de commit desejado

![image](https://user-images.githubusercontent.com/22863027/143615093-66311099-0c6a-4b65-aa5f-ce0d0574a59f.png)

