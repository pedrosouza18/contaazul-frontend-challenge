module.exports = {
  development: {
    API: {
      ROOT: 'https://api.openweathermap.org/data/2.5/weather',
    },
    WEATHER_KEY: 'be27be360acccc99b8794a961a423bfe',
  },
  staging: {
    API: {
      ROOT: 'https://api.openweathermap.org/data/2.5/weather',
    },
    WEATHER_KEY: 'be27be360acccc99b8794a961a423bfe',
  },
  production: {
    API: {
      ROOT: 'https://api.openweathermap.org/data/2.5/weather',
    },
    WEATHER_KEY: 'be27be360acccc99b8794a961a423bfe',
  },
};
