const webpack = require('webpack');
const routes = require('./src/routes');
const envVariables = require('./env/env');

const envMode = process.env.BUILD_ENV || process.env.NODE_ENV || 'development';
const activeEnvKModeKeys = envVariables[envMode];
const envKeys = activeEnvKModeKeys
  ? Object.keys(activeEnvKModeKeys).reduce((acc, envKey) => {
      acc[envKey] = JSON.stringify(activeEnvKModeKeys[envKey]);
      return acc;
    }, {})
  : {};

module.exports = {
  reactStrictMode: true,
  trailingSlash: true,
  distDir: 'dist',
  async rewrites() {
    return routes;
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    config.plugins.push(
      new webpack.DefinePlugin({
        ...envKeys,
      })
    );
    return config;
  },
};
